import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:movies_app/features/show_movies/data/datasource/remote_data_source.dart';
import 'package:movies_app/features/show_movies/data/repositories/get_movies_repository_impl.dart';
import 'package:movies_app/features/show_movies/domain/entities/movie.dart';
import 'package:movies_app/features/show_movies/domain/use_cases/get_actors.dart';
import 'package:movies_app/features/show_movies/domain/use_cases/get_movies.dart';
import 'package:movies_app/features/show_movies/domain/use_cases/get_popular_movies.dart';

import '../../domain/entities/actor.dart';
import '../../domain/use_cases/search_movie.dart';

class HomeProvider with ChangeNotifier {
  List<Movie> popularMovies = [];
  List<Movie> movies = [];
  bool isMovieGettingComplete = false;
  List resultmovies = [];
  List<Movie> listTodisplay = [];
  Movie? selectedMovie;
  List<Actor> actors = [];
  int currentPage = 1;

  bool _isSearching = false;
  bool get isSearching => _isSearching;
  set isSearching(bool value) {
    _isSearching = value;
    notifyListeners();
  }

  Future<void> eitherFailOrGetmovies() async {
    try {
      GetMoviesRepositoryImpl moviesRepositoryImpl =
          GetMoviesRepositoryImpl(remoteDataSource: RemoteDataSourceImpl());
      final failOrGet = await GetMovies(moviesRepositoryImpl).call();

      failOrGet.fold((fail) => log('Error'), (moviesList) {
        if (moviesList != null) {
          movies = moviesList;
          notifyListeners();
        }
      });
    } catch (e) {
      log('Error HomeProvider, EitherFailOrGetMovies $e');
    }
  }

  Future<void> eitherFailOrGetPopularMovies() async {
    try {
      GetMoviesRepositoryImpl moviesRepositoryImpl =
          GetMoviesRepositoryImpl(remoteDataSource: RemoteDataSourceImpl());
      final failOrGet = await GetPopulaMovies(moviesRepositoryImpl)
          .call(currentPage, popularMovies);
      failOrGet.fold((fail) => log('Error'), (popularList) {
        if (popularList != null) {
          popularMovies = [...popularList];
          notifyListeners();
        }
      });
    } catch (e) {
      log('Error HomeProvider, EitherFailOrGetPopularMovies $e');
    }
  }

  Future<void> failOrGetActors(int id) async {
    try {
      GetMoviesRepositoryImpl moviesRepositoryImpl =
          GetMoviesRepositoryImpl(remoteDataSource: RemoteDataSourceImpl());
      final failOrGet = await GetActors(moviesRepositoryImpl).call(id);
      failOrGet.fold((fail) => log('Error'), (actorList) {
        if (actorList != null) {
          actors = actorList;
          notifyListeners();
        }
      });
    } catch (e) {
      log('Error HomeProvider, EitherFailOrGetActors $e');
    }
  }

  clearlist() {
    popularMovies.clear();
    log(popularMovies.length.toString());
    notifyListeners();
  }

  Future<void> FailOrSearchMovie(String query) async {
    try {
      GetMoviesRepositoryImpl repository =
          GetMoviesRepositoryImpl(remoteDataSource: RemoteDataSourceImpl());
      final failOrGet = await SearchMovie(repository).call(query);
      failOrGet.fold((fail) => log('Error'), (listMovies) {
        if (listMovies != null) {
          listTodisplay = listMovies;
          // .where((element) => element.name.toLowerCase().contains(value));
          notifyListeners();
        }
      });
    } catch (e) {
      log('Error HomeProvider, EitherFailOrGetMoviesToSearch $e');
    }
  }
}
