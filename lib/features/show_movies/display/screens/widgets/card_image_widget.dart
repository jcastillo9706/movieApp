import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:movies_app/utils.dart';

import '../../../domain/entities/movie.dart';

class CardImageWidget extends StatelessWidget {
  final String itemName;
  final String imageUrl;
  final double height;
  final double width;
  const CardImageWidget(
      {Key? key,
      required this.itemName,
      required this.imageUrl,
      required this.height,
      required this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    UiUtils uiUtils = UiUtils();
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25)),
      height: height,
      width: width,
      padding: EdgeInsets.symmetric(horizontal: uiUtils.screenWidth * 0.04),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(25),
            child: imageUrl.isEmpty
                ? Image.asset('assets/images/no-image.jpg')
                : CachedNetworkImage(
                    imageUrl: imageUrl,
                    placeholder: (context, image) =>
                        Image.asset('assets/images/loading.gif'),
                    fit: BoxFit.fill,
                  ),
          ),
          const SizedBox(height: 7),
          Center(
            child: Text(
              itemName,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: uiUtils.screenWidth * 0.03,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
}
