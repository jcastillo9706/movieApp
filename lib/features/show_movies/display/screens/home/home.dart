import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:movies_app/features/show_movies/display/providers/homeProvider.dart';
import 'package:movies_app/features/show_movies/display/screens/details/details.dart';
import 'package:movies_app/utils.dart';
import 'package:provider/provider.dart';

import '../widgets/card_image_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ScrollController popularMoviesController = ScrollController();
  late HomeProvider homeProvider;
  bool isEnter = false;

  @override
  void didChangeDependencies() {
    if (!isEnter) {
      isEnter = true;
      homeProvider = Provider.of<HomeProvider>(context);
      popularMoviesController.addListener(() {
        if (popularMoviesController.position.maxScrollExtent ==
            popularMoviesController.position.pixels) {
          homeProvider.currentPage++;
          homeProvider.eitherFailOrGetPopularMovies();

          log('final final');
        }
      });
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    UiUtils uiUtils = UiUtils();
    // homeProvider.eitherFailOrGetmovies();
    return Scaffold(
      appBar: AppBar(actions: [
        CustomAppBar(uiUtils: uiUtils, homeProvider: homeProvider),
      ]),
      body: SafeArea(
        child: SingleChildScrollView(
          child: homeProvider.isSearching
              ? Container(
                  height: uiUtils.screenHeight * 0.4,
                  child: ListView.builder(
                    itemCount: homeProvider.listTodisplay.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(
                            homeProvider.listTodisplay[index].originalTittle),
                        leading: homeProvider
                                .listTodisplay[index].poster.isEmpty
                            ? Image.asset('assets/images/no-image.jpg')
                            : CachedNetworkImage(
                                imageUrl:
                                    homeProvider.listTodisplay[index].poster,
                                width: uiUtils.screenWidth * 0.14,
                              ),
                        onTap: () async {
                          homeProvider.selectedMovie =
                              homeProvider.listTodisplay[index];
                          await homeProvider
                              .failOrGetActors(homeProvider.selectedMovie!.id);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) =>
                                  MoviesDetailsScreen(provider: homeProvider)));
                        },
                      );
                    },
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: uiUtils.screenHeight * 0.03),
                    Center(
                      child: Container(
                        height: uiUtils.screenHeight * 0.5,
                        width: uiUtils.screenWidth * 0.80,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15)),
                        child: Swiper(
                          onTap: (index) async {
                            homeProvider.selectedMovie =
                                homeProvider.movies[index];
                            await homeProvider.failOrGetActors(
                                homeProvider.selectedMovie!.id);
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (_) => MoviesDetailsScreen(
                                      provider: homeProvider,
                                    )));
                            log(homeProvider.movies[index].description);
                          },
                          itemHeight: uiUtils.screenHeight * 0.45,
                          itemWidth: uiUtils.screenWidth * 0.65,
                          layout: SwiperLayout.STACK,
                          indicatorLayout: PageIndicatorLayout.SCALE,
                          itemBuilder: ((context, index) {
                            final image = homeProvider.movies[index].poster;
                            return Hero(
                              tag: '${image}t',
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15),
                                child: image.isEmpty
                                    ? Image.asset('assets/images/no-image.jpg')
                                    : CachedNetworkImage(
                                        imageUrl: image,
                                        fit: BoxFit.cover,
                                        placeholder: (context, image) =>
                                            Image.asset(
                                                'assets/images/loading.gif')),
                              ),
                            );
                          }),
                          itemCount: homeProvider.movies.length,
                        ),
                      ),
                    ),
                    SizedBox(height: uiUtils.screenHeight * 0.03),
                    Container(
                      margin:
                          EdgeInsets.only(left: uiUtils.screenWidth * 0.045),
                      child: const Text(
                        'Populares',
                        textAlign: TextAlign.start,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      height: uiUtils.screenHeight * 0.3,
                      child: ListView.builder(
                          controller: popularMoviesController,
                          itemCount: homeProvider.popularMovies.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, index) {
                            return GestureDetector(
                              child: Hero(
                                tag: homeProvider.popularMovies[index].poster,
                                child: CardImageWidget(
                                    height: uiUtils.screenHeight * 0.3,
                                    width: uiUtils.screenWidth * 0.35,
                                    itemName:
                                        homeProvider.popularMovies[index].name,
                                    imageUrl: homeProvider
                                        .popularMovies[index].poster),
                              ),
                              onTap: () async {
                                homeProvider.selectedMovie =
                                    homeProvider.popularMovies[index];
                                await homeProvider.failOrGetActors(
                                    homeProvider.selectedMovie!.id);
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (_) => MoviesDetailsScreen(
                                          provider: homeProvider,
                                        )));
                              },
                            );
                          }),
                    )
                  ],
                ),
        ),
      ),
    );
  }
}

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({
    Key? key,
    required this.uiUtils,
    required this.homeProvider,
  }) : super(key: key);

  final UiUtils uiUtils;
  final HomeProvider homeProvider;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              width: uiUtils.screenWidth * 0.65,
              child: Padding(
                padding: const EdgeInsets.only(left: 12),
                child: TextField(
                  decoration: const InputDecoration(
                      border: InputBorder.none, hintText: 'eg: Doctor Strange'),
                  onTap: () {
                    homeProvider.isSearching = true;
                  },
                  onChanged: (value) async {
                    await homeProvider.FailOrSearchMovie(value);
                  },
                ),
              ),
            ),
            Positioned(
              right: 0,
              child: IconButton(
                  color: Colors.amber,
                  onPressed: () async {
                    homeProvider.listTodisplay.clear();
                    homeProvider.isSearching = false;
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  icon: !homeProvider.isSearching
                      ? const Icon(Icons.search)
                      : const Icon(Icons.clear)),
            )
          ],
        ),
        SizedBox(
          width: uiUtils.screenWidth * 0.1,
        )
      ],
    );
  }
}
