import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:movies_app/features/show_movies/display/providers/homeProvider.dart';
import 'package:movies_app/utils.dart';

import '../widgets/card_image_widget.dart';

class MoviesDetailsScreen extends StatelessWidget {
  final HomeProvider provider;
  const MoviesDetailsScreen({Key? key, required this.provider})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    UiUtils uiUtils = UiUtils();
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: uiUtils.screenHeight * 0.30,
                child: CachedNetworkImage(
                  imageUrl: provider.selectedMovie!.backDrop,
                  fit: BoxFit.fill,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    margin: EdgeInsets.symmetric(
                        horizontal: uiUtils.screenWidth * 0.04,
                        vertical: uiUtils.screenHeight * 0.03),
                    height: uiUtils.screenHeight * 0.20,
                    width: uiUtils.screenWidth * 0.31,
                    child: Hero(
                      tag: provider.selectedMovie!.poster,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: CachedNetworkImage(
                          imageUrl: provider.selectedMovie!.poster,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: uiUtils.screenWidth * 0.60,
                        child: Text(
                          provider.selectedMovie!.originalTittle,
                          maxLines: 2,
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                          width: uiUtils.screenWidth * 0.40,
                          child: Text(
                            provider.selectedMovie!.name,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          )),
                      // Text('Pelicula'),
                    ],
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: uiUtils.screenWidth * 0.03),
                child: Text(
                  provider.selectedMovie!.description,
                ),
              ),
              SizedBox(height: 40),
              Container(
                height: uiUtils.screenHeight * 0.30,
                child: provider.popularMovies.isEmpty
                    ? Text('No info')
                    : ListView.builder(
                        itemCount: provider.actors.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, index) {
                          return CardImageWidget(
                              height: uiUtils.screenHeight * 0.20,
                              width: uiUtils.screenWidth * 0.30,
                              itemName: provider.actors[index].name,
                              imageUrl: provider.actors[index].photo);
                        }),
              )
            ],
          ),
        ),
      )),
    );
  }
}
