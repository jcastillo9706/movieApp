import 'package:dartz/dartz.dart';
import 'package:movies_app/features/show_movies/domain/entities/movie.dart';
import 'package:movies_app/features/show_movies/domain/repositories/get_movies_repository.dart';

class GetMovies {
  final MoviesRepository moviesRepository;

  GetMovies(this.moviesRepository);
  Future<Either<Map<String, dynamic>, List<Movie>?>> call() async {
    return moviesRepository.getMovies();
  }
}
