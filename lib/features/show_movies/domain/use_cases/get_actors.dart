import 'package:dartz/dartz.dart';
import 'package:movies_app/features/show_movies/domain/repositories/get_movies_repository.dart';

import '../entities/actor.dart';

class GetActors {
  final MoviesRepository moviesRepository;

  GetActors(this.moviesRepository);
  Future<Either<Map<String, dynamic>, List<Actor>?>> call(int id) async {
    return moviesRepository.getActors(id);
  }
}
