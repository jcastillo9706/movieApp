import 'package:dartz/dartz.dart';
import 'package:movies_app/features/show_movies/domain/entities/movie.dart';
import 'package:movies_app/features/show_movies/domain/repositories/get_movies_repository.dart';

class SearchMovie {
  final MoviesRepository repository;

  SearchMovie(this.repository);
  Future<Either<Map<String, dynamic>, List<Movie>?>> call(String query) async {
    return repository.getAllMovies(query);
  }
}
