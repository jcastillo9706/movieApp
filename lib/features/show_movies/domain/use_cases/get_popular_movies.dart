import 'package:dartz/dartz.dart';
import 'package:movies_app/features/show_movies/domain/entities/movie.dart';
import 'package:movies_app/features/show_movies/domain/repositories/get_movies_repository.dart';

class GetPopulaMovies {
  final MoviesRepository repository;

  GetPopulaMovies(this.repository);
  Future<Either<Map<String, dynamic>, List<Movie>?>> call(
      int page, List<Movie> currenPopularMovies) async {
    return repository.getPopularMovies(page, currenPopularMovies);
  }
}
