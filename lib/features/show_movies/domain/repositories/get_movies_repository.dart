import 'package:dartz/dartz.dart';
import 'package:movies_app/features/show_movies/domain/entities/actor.dart';
import 'package:movies_app/features/show_movies/domain/entities/movie.dart';

abstract class MoviesRepository {
  Future<Either<Map<String, dynamic>, List<Movie>?>> getMovies();
  Future<Either<Map<String, dynamic>, List<Movie>?>> getPopularMovies(
      int page, List<Movie> currenPopularMovies);
  Future<Either<Map<String, dynamic>, List<Actor>?>> getActors(int id);
  Future<Either<Map<String, dynamic>, List<Movie>?>> getAllMovies(String query);
}
