class Movie {
  final String name;
  final int id;
  final String originalTittle;
  final String description;
  final String poster;
  final String backDrop;

  Movie({
    required this.name,
    required this.id,
    required this.originalTittle,
    required this.description,
    required this.poster,
    required this.backDrop,
  });
}
