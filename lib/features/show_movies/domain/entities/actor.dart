class Actor {
  final String name;
  final String photo;

  Actor({required this.name, required this.photo});
}
