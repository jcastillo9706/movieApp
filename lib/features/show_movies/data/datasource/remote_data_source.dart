import 'dart:convert';
import 'dart:developer';

import 'package:movies_app/features/show_movies/data/models/actor.dart';
import 'package:movies_app/features/show_movies/data/models/movie.dart';
import 'package:movies_app/features/show_movies/display/providers/homeProvider.dart';
import 'package:movies_app/features/show_movies/domain/entities/actor.dart';

import '../../domain/entities/movie.dart';
import 'package:http/http.dart' as http;

abstract class RemoteDataSource {
  Future<List<Movie>?> getMovies();
  Future<List<Movie>?> getPopularMovies(
      int page, List<Movie> currenPopularMovies);
  Future<List<Actor>?> getCharacters(int id);
  Future<List<Movie>?> getallMovies(String query);
}

class RemoteDataSourceImpl implements RemoteDataSource {
  @override
  Future<List<Movie>?> getMovies() async {
    try {
      List<Movie> movies = [];

      Uri api = Uri.parse(
          'https://api.themoviedb.org/3/movie/now_playing?api_key=ac51fde67b1f2adb471dc3cbe3f33e69&language=es-ES&page=1');
      http.Response res = await http.get(api);
      if (res.statusCode != 200) return null;
      Map<String, dynamic> decoded = jsonDecode(res.body);
      log(decoded['results'][0].runtimeType.toString());
      for (Map<String, dynamic> movieMap in decoded['results']) {
        movies.add(MovieModel.fromMap(movieMap));
      }

      return movies;
    } catch (e) {
      log(e.toString());
    }
    return null;
  }

  @override
  Future<List<Movie>?> getPopularMovies(
      int page, List<Movie> currenPopularMovies) async {
    try {
      List<Movie> popularMovies = [...currenPopularMovies];

      Uri api = Uri.parse(
          'https://api.themoviedb.org/3/movie/popular?api_key=ac51fde67b1f2adb471dc3cbe3f33e69&language=es-ES&page=$page');
      http.Response res = await http.get(api);
      if (res.statusCode != 200) return null;
      Map<String, dynamic> decoded = jsonDecode(res.body);
      for (Map<String, dynamic> movieMap in decoded['results']) {
        popularMovies.add(MovieModel.fromMap(movieMap));
      }

      return popularMovies;
    } catch (e) {
      log(e.toString());
    }
    return null;
  }

  @override
  Future<List<Actor>?> getCharacters(int id) async {
    try {
      List<Actor> actors = [];
      Uri url = Uri.parse(
          'https://api.themoviedb.org/3/movie/$id/credits?api_key=ac51fde67b1f2adb471dc3cbe3f33e69&language=es-ES');
      http.Response res = await http.get(url);
      if (res.statusCode != 200) null;
      Map<String, dynamic> decoded = jsonDecode(res.body);
      for (Map<String, dynamic> movieActor in decoded['cast']) {
        actors.add(ActorModel.fromMap(movieActor));
      }
      return actors;
    } catch (e) {
      log(e.toString());
    }
    return null;
  }

  @override
  Future<List<Movie>?> getallMovies(String query) async {
    try {
      List<Movie> listTodisplay = [];
      Uri uri = Uri.parse(
          'https://api.themoviedb.org/3/search/movie?api_key=ac51fde67b1f2adb471dc3cbe3f33e69&language=es-ES&query=$query');
      http.Response res = await http.get(uri);
      if (res.statusCode != 200) return null;
      Map<String, dynamic> decoded = jsonDecode(res.body);
      for (Map<String, dynamic> movieToSearch in decoded['results']) {
        listTodisplay.add(MovieModel.fromMap(movieToSearch));
      }
      return listTodisplay;
    } catch (e) {
      log(e.toString());
    }
    return null;
  }
}
