import 'dart:developer';

import 'package:movies_app/features/show_movies/data/datasource/remote_data_source.dart';
import 'package:movies_app/features/show_movies/domain/entities/actor.dart';
import 'package:movies_app/features/show_movies/domain/entities/movie.dart';
import 'package:dartz/dartz.dart';
import 'package:movies_app/features/show_movies/domain/repositories/get_movies_repository.dart';

class GetMoviesRepositoryImpl implements MoviesRepository {
  final RemoteDataSource remoteDataSource;

  GetMoviesRepositoryImpl({required this.remoteDataSource});
  @override
  Future<Either<Map<String, dynamic>, List<Movie>?>> getMovies() async {
    try {
      return Right(await remoteDataSource.getMovies());
    } catch (e) {
      return left({'status': 'fail', 'error': '$e'});
    }
  }

  @override
  Future<Either<Map<String, dynamic>, List<Movie>?>> getPopularMovies(
      int page, List<Movie> currenPopularMovies) async {
    try {
      return Right(
          await remoteDataSource.getPopularMovies(page, currenPopularMovies));
    } catch (e) {
      return Left({'status': 'fail', 'error': '$e'});
    }
  }

  @override
  Future<Either<Map<String, dynamic>, List<Actor>?>> getActors(int id) async {
    try {
      return Right(await remoteDataSource.getCharacters(id));
    } catch (e) {
      return Left({'Status': 'fail', 'error': '$e'});
    }
  }

  @override
  Future<Either<Map<String, dynamic>, List<Movie>?>> getAllMovies(
      String query) async {
    try {
      return Right(await remoteDataSource.getallMovies(query));
    } catch (e) {
      return Left({'Status': 'fail', 'error': '$e'});
    }
  }
}
