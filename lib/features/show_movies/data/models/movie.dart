import 'package:movies_app/features/show_movies/domain/entities/movie.dart';
import 'package:movies_app/utils.dart';

class MovieModel extends Movie {
  MovieModel({
    required super.name,
    required super.id,
    required super.originalTittle,
    required super.description,
    required super.poster,
    required super.backDrop,
  });

  factory MovieModel.fromMap(Map<String, dynamic> map) {
    return MovieModel(
        name: map['title'],
        id: map['id'],
        originalTittle: map['original_title'],
        description: map['overview'],
        poster: map['poster_path'] != null
            ? UiUtils().baseUrlImg + map['poster_path']
            : '',
        backDrop: map['backdrop_path'] != null
            ? UiUtils().baseUrlImg + map['backdrop_path']
            : '');
  }

  // static Map<String, dynamic> toMap(Movie movie) {
  //   return {'resulst': movie.results};
  // }
}
