import 'package:movies_app/features/show_movies/domain/entities/actor.dart';
import 'package:movies_app/utils.dart';

class ActorModel extends Actor {
  ActorModel({
    required super.name,
    required super.photo,
  });
  factory ActorModel.fromMap(Map<String, dynamic> map) {
    return ActorModel(
        name: map['name'],
        photo: map['profile_path'] != null
            ? UiUtils().baseUrlImg + map['profile_path']
            : '');
  }
}
