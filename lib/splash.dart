import 'dart:async';

import 'package:flutter/material.dart';
import 'package:movies_app/features/show_movies/display/providers/homeProvider.dart';
import 'package:movies_app/features/show_movies/display/screens/home/home.dart';
import 'package:movies_app/utils.dart';
import 'package:provider/provider.dart';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isLoaded = false;
  UiUtils uiUtils = UiUtils();
  late HomeProvider homeProvider;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Center(
              child: GestureDetector(
                child: const Text(
                  'movies',
                  style: TextStyle(fontSize: 27, fontWeight: FontWeight.w500),
                ),
                onTap: () async {
                  await homeProvider.eitherFailOrGetmovies();
                  await homeProvider.eitherFailOrGetPopularMovies();
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => const HomeScreen()));
                },
              ),
            ),
          ),
          LottieBuilder.asset('assets/images/tap.json')
        ],
      ),
    );
  }

  @override
  void didChangeDependencies() {
    if (!isLoaded) {
      isLoaded = true;
      uiUtils.getDeviceSize(context: context);
      const Duration(seconds: 2);
      homeProvider = Provider.of<HomeProvider>(context);
    }
    super.didChangeDependencies();
  }
}
