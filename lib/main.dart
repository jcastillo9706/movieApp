import 'package:flutter/material.dart';
import 'package:movies_app/features/show_movies/display/providers/homeProvider.dart';
import 'package:movies_app/features/show_movies/display/screens/home/home.dart';
import 'package:movies_app/splash.dart';
import 'package:movies_app/utils.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => HomeProvider())
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: SplashScreen(),
        ));
  }
}
