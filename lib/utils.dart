import 'package:flutter/cupertino.dart';

class UiUtils {
  UiUtils._privateConstructor();
  static final UiUtils _instance = UiUtils._privateConstructor();
  factory UiUtils() {
    return _instance;
  }
  double screenWidth = 0;
  double screenHeight = 0;
  String baseUrlImg = 'https://image.tmdb.org/t/p/w500';

  void getDeviceSize({required BuildContext context}) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
  }
}
